package arquivos.imd.ufrn.br.arquivosandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ListActivity extends Activity {

    private ListView arquivosView;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        arquivosView = (ListView) findViewById(R.id.listArquivoView);
        File[] arquivos = getFilesDir().listFiles();
        List<String> nomesArquivos = new ArrayList<String>();

        for (int i = 0; i < arquivos.length; i++) {
            nomesArquivos.add(arquivos[i].getName());
        }
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nomesArquivos);
        arquivosView.setAdapter(adapter);
        arquivosView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String item = (String) adapter.getItem(i);
                Intent intent = new Intent(ListActivity.this, DetailActivity.class);
                intent.putExtra("arquivo", item);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
