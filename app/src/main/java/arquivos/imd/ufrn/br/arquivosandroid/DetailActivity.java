package arquivos.imd.ufrn.br.arquivosandroid;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class DetailActivity extends Activity {

    private TextView detalheText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        detalheText = (TextView) findViewById(R.id.detalheText);

        String nomeArquivo = getIntent().getStringExtra("arquivo");
        FileInputStream fis = null;
        try {
            fis = openFileInput(nomeArquivo);
            int c;
            String texto="";
            while( (c = fis.read()) != -1){
                texto = texto + Character.toString((char)c);
            }
            fis.close();
            detalheText.setText(texto);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
