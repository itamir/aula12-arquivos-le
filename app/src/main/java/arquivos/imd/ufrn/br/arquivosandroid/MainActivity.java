package arquivos.imd.ufrn.br.arquivosandroid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.io.FileOutputStream;
import java.io.IOException;


public class MainActivity extends Activity {

    private EditText informacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        informacao = (EditText) findViewById(R.id.informacaoText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void salvar(View view) {

        int id = getFilesDir().listFiles().length + 1;

        String arquivo = id + ".txt";
        String texto= informacao.getText().toString();
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(arquivo, Context.MODE_PRIVATE);
            fos.write(texto.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent i = new Intent(this, ListActivity.class);
        i.putExtra("total", "Total: " + id + " registros." );
        startActivity(i);
    }
}
